CREATE DATABASE IF NOT EXISTS icaphe;

CREATE TABLE IF NOT EXISTS tables (
    table_id INT AUTO_INCREMENT NOT NULL PRIMARYKEY,
    table_name VARCHAR (10),
    cofee_shop_id INT NOT NULL
);

CREATE TABLE IF NOT EXISTS payment (
    payment_id INT AUTO_INCREMENT NOT NULL PRIMARYKEY,
    oder_id INT NOT NULL,
    update_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    payment_type VARCHAR (50),
    payment_status INT,
    user_id INT
);

CREATE TABLE IF NOT EXISTS payment_history (
    payment_history_id INT AUTO_INCREMENT NOT NULL PRIMARYKEY,
    payment_id INT NOT NULL,
    oder_id INT NOT NULL,
    update_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    payment_type VARCHAR (50),
    payment_status INT,
    user_id INT
);
