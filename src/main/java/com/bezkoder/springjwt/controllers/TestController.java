package com.bezkoder.springjwt.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
//	@GetMapping("/all")
//	public String allAccess() {
//		return "Public Content.";
//	}
//
//	@GetMapping("/user")
//	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
//	public String userAccess() {
//
//		return "User Content.";
//	}
//
//	@GetMapping("/mod")
//	@PreAuthorize("hasRole('MODERATOR')")
//	public String moderatorAccess() {
//		return "Moderator Board.";
//	}
//
//	@GetMapping("/admin")
//	@PreAuthorize("hasRole('ADMIN')")
//	public String adminAccess() {
//		return "Admin Board.";
//	}
@GetMapping("/user_info")
public ResponseEntity<?> getUserInfoFromToken(@AuthenticationPrincipal UserDetails userDetails){
	if(userDetails==null){
		return new ResponseEntity<String>("Unauthorized", HttpStatus.UNAUTHORIZED);
//			ResponseEntity.status(HttpStatus.BAD_REQUEST).body(exception.getMsg());
	}
	Map<Object, Object> model = new HashMap<>();
	model.put("password", userDetails.getPassword().hashCode());
	model.put("username", userDetails.getUsername());
	return ResponseEntity.ok(model);
}
}
