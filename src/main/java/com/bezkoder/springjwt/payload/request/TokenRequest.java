package com.bezkoder.springjwt.payload.request;

import lombok.Getter;

import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class TokenRequest {
    @NotBlank
    @Size(min = 3, max = 20)
    @Getter
    @Setter
    private String token;

}
